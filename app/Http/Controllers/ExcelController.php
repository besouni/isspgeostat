<?php

namespace App\Http\Controllers;

use App\Http\Custom\ExcelWorker;
use Illuminate\Http\Request;

use Excel;

class ExcelController extends Controller
{
    //
    public function excel(){
        return Excel::download(new ExcelWorker, "data.xlsx");
    }
}
