<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;

class MailController extends Controller
{
    //
//    public $mails = []
    public function mail1(){
//        dd(12);
        Mail::send([], [], function($message) {
            $message ->from('besotabata@gmail.com','Beso Tabatadze')
                ->to('besotabatadze84@gmail.com', 'Laravel Test')
                ->subject('Laravel Basic Testing Mail');
        });
        return "<h1>Mail sent </h1>";
    }

    public function mail2(){
//        dd(12);
//        return view('mail');
        $data = array('name'=>"Beso");
        Mail::send('mails.mail', $data, function($message) {
            $message ->from('besotabata@gmail.com','Beso Tabatadze')
                ->to(['besotabatadze84@gmail.com', 'spirtskhalava@geostat.ge', 'ogudadze@geostat.ge'], 'Laravel Test')
                ->subject('Laravel Basic Testing Mail');
        });
        return "<h1>Mail sent </h1>";
    }
}
