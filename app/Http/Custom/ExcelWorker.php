<?php

namespace App\Http\Custom;

use App\User;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExcelWorker implements FromCollection, WithEvents, WithHeadings
{
    public function collection()
    {
        return User::get(['email', 'password', 'name', 'last_name']);
    }

    public function headings(): array
    {
        $headers = array("email", "password", "name", "lastname");
        return $headers;
    }

    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:G1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'size' => 14
                    ]
                ]);
            },
        ];
    }
}
