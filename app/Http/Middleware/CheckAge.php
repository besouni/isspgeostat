<?php

namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd($request->age);
        if($request->age < 18 ){
//            dd($request->age);
//            return redirect('/age1');
            return redirect(route('age1'));
        }
        return $next($request);
    }
}
