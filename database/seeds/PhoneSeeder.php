<?php

use Illuminate\Database\Seeder;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i=1; $i<=7; $i++) {
            DB::table('phones')->insert(
                [
                    'mobile' => rand(100000000, 999999999),
                    'user_id' => $i
                ]
            );
        }
    }
}
