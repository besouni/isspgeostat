<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rules')->insert(
            [
                [
                    'name' => 'admin'
                ],
                [
                    'name' => 'moderator'
                ],
                [
                    'name' => 'auth'
                ],
                [
                    'name' => 'guest'
                ]
            ]
        );
    }
}
