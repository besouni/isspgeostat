<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PostController;

Route::get('/age/{age}', function ($age) {
    return "AGE  - ".$age;
})->middleware('CheckAge');

Route::get('/age1/', function () {
    return "AGE1  - ";
})->name("age1");


//Route::get('post', 'PostController@index');
//Route::resource('post', PostController::class);
Route::resource('post', 'PostController');

Route::prefix('contr')->group(function () {
    Route::get('/', 'PostTest1@test1');
    Route::get('/test2', [\App\Http\Controllers\PostTest1::class, 'test2']);
});


Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return "Admin  - ";
    });
    Route::get('/user', function () {
        return "User  - ";
    });
    Route::get('/profile', function () {
        return "Profile  - ";
    });
});



