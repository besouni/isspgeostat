<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/child', function () {
    return view('child');
});


Route::get('/test1/{name?}', function ($name="user") {
    return "Test 1 - ".$name;
});

Route::get('/test1/{name?}/info', function ($name="user") {
    return "Test Info - ".$name;
});

Route::get('/view4', function () {
    return view('mail');
});


Route::get('/', function () {
    return view('welcome');
});

Route::prefix('eloquent')->group(function () {
    Route::get('/', [\App\Http\Controllers\EloquentController::class, 'index']);
});

Route::get('/mail1',  [\App\Http\Controllers\MailController::class, 'mail1']);
Route::get('/mail2',  [\App\Http\Controllers\MailController::class, 'mail2']);
Route::get('/excel',  [\App\Http\Controllers\ExcelController::class, 'excel']);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
